FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://auto.automnt \
		file://auto.master"

do_install_append () {
	install -d ${D}${sysconfdir}
	install -m 0640 ${WORKDIR}/auto.automnt ${D}${sysconfdir}
	echo "/automnt /etc/auto.automnt --timeout=5 --ghost" >> ${D}${sysconfdir}/auto.master
}

FILES_${PN} += "${sysconfdir}"

