FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"

SRC_URI += "file://inittab \
           "

USE_VT = "0"
SYSVINIT_ENABLED_GETTYS = "0"

do_install_append () {
	install -d 0640 ${D}${sysconfdir}
	install -m 0640 ${WORKDIR}/inittab ${D}${sysconfdir}/inittab
}
