
require recipes-core/images/core-image-minimal.bb

LICENSE="GPLv2"

IMAGE_INSTALL += " \
	omxplayer \
	dropbear \
	python \
	python-subprocess \
	git \
	autofs \
	musicplayer \
	"

#	python-evdev \
#	mesa-gl \
#	dbus \
#	python-dbus \
#	pyomx \
#	wpa-supplicant \
#	mpg123 \
#	mpd \
#	alsa-utils \
#	alsa-dev \
#	alsa-lib \
#	alsa-utils-scripts \
#	pulseaudio \
#

do_not_start_getty () {
	sed '/getty/ s/^/# /' ${IMAGE_ROOTFS}${sysconfdir}/inittab
}

#ROOTFS_POSTPROCESS_COMMAND += "do_not_start_getty ; "
#IMAGE_PREPROCESS_COMMAND += "do_not_start_getty ; "
